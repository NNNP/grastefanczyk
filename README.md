# Gra Labirynt

Wydostań się z labiryntu z pomocą wskazówek od przewodnika.

# Opis

W labiryncie został uwięziony **poszukiwacz** skarbów musi od odnaleźć wyjście. W ucieczce pomaga mu **przewodnik** znajdujący się w pomieszczeniu ze stołem, na którym leży mapa labiryntu. **Przewodnik** wie, w którym miejscu zaczyna **poszukiwacz**. **Poszukiwacz** w labiryncie natrafia na bloki, na których gdy stanie ukazuje swoje połorzenie na mapie **przewodnika**. **Poszukiwacz i przewodnik** prowadzą ze sobą rozmowę za pomocą run. Sami muszą ustalić co oznacza dany symbol (np. Koło = skręć w przwo). Gra kończy się gdy **poszukiwacz** wydostanie się z labiryntu.

# Sterowanie

-   Poszukiwacz – klawiatura (WASD)
-   Przewodnik – myszka

# Wymagania przed odpaleniem

1. Musisz posiadać serwer **MongoDb** i wpisać go do zmiennej url w pliku server.coffee
2. Zalecane jest posiadanie **yarna**, jednak każde polecenie jest możliwe do zastąpienia poleceniem npm np. **npm start** lub **npm run compile** 

# Jak uruchomić grę

1.  Należy dostać się do folderu **/path/to/client\_react** w naszym projekcie i tam odpalić **yarn install && yarn compile && yarn build**
2.  Następnie w główny folderze projektu **yarn install && yarn start**
