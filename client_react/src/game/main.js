import $ from 'jquery';
const THREE = require('three')
import Wall from './static/js/wall'
// import C
export default function Main(ala, socket, id) {
  var scene = new THREE.Scene();
  var raycaster = new THREE.Raycaster();
  var raycaster2 = new THREE.Raycaster();
  var raycaster3 = new THREE.Raycaster();
  var camera = new THREE.PerspectiveCamera(
    110,
    4 / 3,
    0.1,
    1000
  );
  var renderer = new THREE.WebGLRenderer();
  renderer.setClearColor(0xffffff);
  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.PCFSoftShadowMap;
  renderer.setSize(window.innerWidth, window.innerHeight);
  $("#root").append(renderer.domElement);
  var axes = new THREE.AxesHelper(1000)
  scene.add(axes)
  camera.position.set(-100, 50, -100)

  this.timer = function() {
    var countDownDate = new Date().getTime()+ 4*60*1000;

    // Update the count down every 1 second
    var x = setInterval(function() {

      // Get todays date and time
      var now = new Date().getTime();

      // Find the distance between now an the count down date
      var distance = countDownDate - now;

      // Time calculations for days, hours, minutes and seconds
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // Display the result in the element with id="demo"
      minutes = 0
      ala.html(minutes + ":" + seconds)
      if(ala.html() == 0 + ":" + '00')
      {
        clearInterval(x);
        alert("koniec czasu")
        nie = true
      }
     // console.log(minutes + ":" + seconds);
      // If the count down is finished, write some text
      if (distance < 0) {
        clearInterval(x);
        document.getElementById("demo").innerHTML = "EXPIRED";
      }
    }, 1000);
  }

  //wall
  var wall = new Wall();
  var con = wall.getWallCont();
  con.position.x = 150
  scene.add(con)
  function Field(){
    this.x = 0
    this.z = 0
  }
  var actualfield = new Field()
  var conteners = []
  var stop = 0;
  var stop2 = 0;
  var blue = []
  var map = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1],
    [3, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  ]

  for (var i = 0; i < map.length; i++) {
    for (var j = 0; j < map[i].length; j++) {
      console.log(map[j][i]);
      if (map[j][i] == 1) {
        var wall = new Wall();
        var con = wall.getWallCont();
        con.position.set(i * 100, 0, j * 100)
        scene.add(con)
        conteners.push(con)
      }
      else if (map[j][i] == 2) {
        var geometryc = new THREE.BoxGeometry(100, 0, 100);
        var materialc = new THREE.MeshBasicMaterial({
          side: THREE.DoubleSide,
          map: new THREE.TextureLoader().load('static/mats/diamond_stone.png'),
        });
        var plane2 = new THREE.Mesh(geometryc, materialc);
        //plane2.rotation.x = Math.PI / 2
        scene.add(plane2);
        plane2.position.set(i * 100 + 25, -25, j * 100 + 25)
        blue.push(plane2)
        conteners.push(plane2)
        //conteners.push(plane)
      }
      else if (map[j][i] == 0) {
        var geometryp = new THREE.PlaneGeometry(100, 100, 20);
        var materialp = new THREE.MeshBasicMaterial({
          side: THREE.DoubleSide,
          map: new THREE.TextureLoader().load('static/mats/moss_stone.png'),
        });
        var plane = new THREE.Mesh(geometryp, materialp);
        plane.rotation.x = Math.PI / 2
        scene.add(plane);
        plane.position.set(i * 100 + 25, -25, j * 100 + 25)
        //conteners.push(plane)
      }
      else if (map[j][i] == 3) {
        //console.log(camera);
        camera.position.set(i * 100, 50, j * 100 + 20)
        actualfield.x = i-1
        actualfield.z = j
        console.log(actualfield)
      }
      else {
        console.log("blad mapy");
      }
    }
  }
  //console.log("OTO TAB: ", conteners);
  var nie = false
  var direction = 0;
  if(nie != true){
  $(document).keydown(function(e) {
    //console.log(e.which);
    if (e.which == 68 && nie != true) {
      var u = 0;
      var i = 0;
      var int = setInterval(function() {
        if (u == 18) {
          clearInterval(int)
        }
        if (u >= 9) {
          i--
        } else {
          i++
        }
        u++
        camera.rotation.y -= (0.5 + i) * Math.PI / 180
      }, 10);
      if (direction != 0)
        direction--
        else
          direction = 3
    } else if (e.which == 65 && nie != true) {
      var u = 0;
      var i = 0;
      var int = setInterval(function() {
        if (u == 18) {
          clearInterval(int)
        }
        if (u >= 9) {
          i--
        } else {
          i++
        }
        u++
        camera.rotation.y += (0.5 + i) * Math.PI / 180
      }, 10);
      if (direction != 3)
        direction++
        else
          direction = 0
    }
    else if (e.which == 87 && stop != 1 && nie != true) {
      if (direction == 3) {
        var u = 0;
        var i = 0;
        var int = setInterval(function() {
          if (u == 15 || stop == 1) {
            clearInterval(int)
          }
          if (u >= 8) {
            i--
          } else {
            i++
          }
          u++
          camera.position.x += 0.5 + i
        }, 20)
        actualfield.x += 1
        console.log(actualfield.x,actualfield.z)
        if(map[actualfield.x][actualfield.z] == 2){
          alert("CHEKCPOINT!!!")
          socket.emit('checkpoint', { id: id, x: actualfield.x, z: actualfield.z})
        }

      } else if (direction == 1) {
        var u = 0;
        var i = 0;
        var int = setInterval(function() {
          if (u == 15 || stop == 1) {
            clearInterval(int)
          }
          if (u >= 8) {
            i--
          } else {
            i++
          }
          u++
          camera.position.x -= 0.5 + i
        }, 20);
        actualfield.x -= 1
        if(map[actualfield.x][actualfield.z] == 2){
          alert("CHEKCPOINT!!!")
        }
        if(map[actualfield.x][actualfield.z] == 4){
          alert("KONIEC GRY")
        }
        console.log(actualfield.x,actualfield.z)
      } else if (direction == 2) {
        var u = 0;
        var i = 0;
        var int = setInterval(function() {
          if (u == 15 || stop == 1) {
            clearInterval(int)
          }
          if (u >= 8) {
            i--
          } else {
            i++
          }
          u++
          camera.position.z += 0.5 + i
        }, 20);
        actualfield.z += 1
        if(map[actualfield.x][actualfield.z] == 2){
          alert("CHEKCPOINT!!!")
        }
        if(map[actualfield.x][actualfield.z] == 4){
          alert("KONIEC GRY")
        }
        console.log(actualfield.x,actualfield.z)
      } else if (direction == 0) {
        var u = 0;
        var i = 0;
        var int = setInterval(function() {
          if (u == 15 || stop == 1) {
            clearInterval(int)
          }
          if (u >= 8) {
            i--
          } else {
            i++
          }
          u++
          camera.position.z -= 0.5 + i
        }, 20);
        actualfield.z -= 1
        if(map[actualfield.x][actualfield.z] == 2){
          alert("CHEKCPOINT!!!")
        }
        if(map[actualfield.x][actualfield.z] == 4){
          alert("KONIEC GRY")
        }
        console.log(actualfield.x,actualfield.z)
      }
      if(map[actualfield.x][actualfield.z] == 2){
        alert("CHEKCPOINT!!!")
      }
    }
    else if (e.which == 83 && stop2 != 1 && nie != true) {
      if (direction == 3) {
        var u = 0;
        var i = 0;
        var int = setInterval(function() {
          if (u == 15) {
            clearInterval(int)
          }
          if (u >= 8 || stop2 == 1) {
            i--
          } else {
            i++
          }
          u++
          camera.position.x -= 0.5 + i
        }, 20);
        actualfield.x -= 1
        if(map[actualfield.x][actualfield.z] == 2){
          alert("CHEKCPOINT!!!")
        }
        if(map[actualfield.x][actualfield.z] == 4){
          alert("KONIEC GRY")
        }
      } else if (direction == 1) {
        var u = 0;
        var i = 0;
        var int = setInterval(function() {
          if (u == 15 || stop2 == 1) {
            clearInterval(int)
          }
          if (u >= 8) {
            i--
          } else {
            i++
          }
          u++
          camera.position.x += 0.5 + i
        }, 20);
        actualfield.x += 1
        if(map[actualfield.x][actualfield.z] == 2){
          alert("CHEKCPOINT!!!")
        }
        if(map[actualfield.x][actualfield.z] == 4){
          alert("KONIEC GRY")
        }
        console.log(actualfield.x,actualfield.z)
      } else if (direction == 2) {
        var u = 0;
        var i = 0;
        var int = setInterval(function() {
          if (u == 15 || stop2 == 1) {
            clearInterval(int)
          }
          if (u >= 8) {
            i--
          } else {
            i++
          }
          u++
          camera.position.z -= 0.5 + i
        }, 20);
        
        actualfield.z -= 1
        if(map[actualfield.x][actualfield.z] == 2){
          alert("CHEKCPOINT!!!")
        }
        if(map[actualfield.x][actualfield.z] == 4){
          alert("KONIEC GRY")
        }
        console.log(actualfield.x,actualfield.z)
      } else if (direction == 0) {
        var u = 0;
        var i = 0;
        var int = setInterval(function() {
          if (u == 15 || stop2 == 1) {
            clearInterval(int)
          }
          if (u >= 8) {
            i--
          } else {
            i++
          }
          u++
          camera.position.z += 0.5 + i
        }, 20);
        actualfield.z += 1
        if(map[actualfield.x][actualfield.z] == 2){
          alert("CHEKCPOINT!!!")
        }
        if(map[actualfield.x][actualfield.z] == 4){
          alert("KONIEC GRY")
        }
      }
    }

  })
}
  this.render = function() {
    var wekt1 = new THREE.Vector3(camera.position.x, camera.position.y+20, camera.position.z)
    var wekt2 = new THREE.Vector3(camera.position.x, camera.position.y-2000, camera.position.z)
    var wektempty = new THREE.Vector3()
    var ray = new THREE.Ray(new THREE.Vector3(camera.position.x, camera.position.y, camera.position.z), camera.getWorldDirection())
    var ray2 = new THREE.Ray(new THREE.Vector3(camera.position.x, camera.position.y, camera.position.z), camera.getWorldDirection().negate())
    //var ray3 = new THREE.Ray(new THREE.Vector3(camera.position.x, camera.position.y+100, camera.position.z), new THREE.Vector3(camera.position.x, camera.position.y-200, camera.position.z).normalize())
    var ray3 = new THREE.Ray(wekt1,wektempty.subVectors(wekt1,wekt2).normalize())
    raycaster.ray = ray
    raycaster2.ray = ray2
    raycaster3.ray = ray3
    var intersects = raycaster.intersectObjects(conteners, true);
    if (intersects[0]) {

      //console.log(intersects[0].distance) // odległość od vertex-a na wprost, zgodnie z kierunkiem ruchu
      // boxik.position.x = intersects[0].point.x
      // boxik.position.y = intersects[0].point.y
      // boxik.position.z = intersects[0].point.z

      if (intersects[0].distance <= 65) {
        stop = 1;
      } else {
        stop = 0;
      }
    }
    var intersects2 = raycaster2.intersectObjects(conteners, true);
    if (intersects2[0]) {

      //console.log(intersects2[0].distance) // odległość od vertex-a na wprost, zgodnie z kierunkiem ruchu

      if (intersects2[0].distance <= 65) {
        stop2 = 1;
      } else {
        stop2 = 0;
      }
    }
    var intersects3 = raycaster3.intersectObjects(blue, true);
    //console.log(blue);
    if (intersects3[0]) {
      // blue.forEach(function(val){
      //   if(intersects[0].uuid == val.uuid)
      //   {
      //     var tmp = conteners.indexOf(val);
      //     console.log(tmp,conteners);
      //   }
      // })
      console.log(intersects3[0].distance) // odległość od vertex-a na wprost, zgodnie z kierunkiem ruchu

    //  if (intersects3[0].distance <= 50) {
        alert("CHECKPOINT!!!")
    //  }
    }

    requestAnimationFrame(this.render);
    renderer.render(scene, camera);
    plane.receiveShadow = true;


  }.bind(this)
  this.render();


}
