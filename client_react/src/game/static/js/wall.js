import THREE from 'three';
import { $, jQuery } from 'jquery';
const THREE = require('three');
import moss_stone from '../mats/moss_stone.png'

export default function Wall() {

    var container = new THREE.Object3D()

    var geometry = new THREE.BoxGeometry(50, 50, 50);
    var material = new THREE.MeshBasicMaterial({
        side: THREE.DoubleSide,
        map: new THREE.TextureLoader().load('static/mats/moss_stone.png'),
        transparent: false,
        opacity: 0.0
    });
    for(var i=0;i<8;i++){
        var wall = new THREE.Mesh(geometry, material); // wall sześcian
        if(i%2==1)wall.position.y = 50;
        if(i==2|i==3|i==6|i==7)wall.position.x = 50;
        if(i>3)wall.position.z=50;
        container.add(wall)
    }
    

    //funkcja zwracająca kontener
    this.getWallCont = function () {
        return container
    }
}