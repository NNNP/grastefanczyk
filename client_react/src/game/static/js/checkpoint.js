import { $, jQuery } from 'jquery';
import THREE from 'three';
export default function Checkpoint() {

    var container = new THREE.Object3D()

    var geometry = new THREE.BoxGeometry(50, 50, 50);

    var materials = [];

    materials.push(new THREE.MeshBasicMaterial({ side: THREE.DoubleSide, map: new THREE.TextureLoader().load('static/mats/moss_stone.png'),transparent: true,opacity: 0.0}));
    materials.push(new THREE.MeshBasicMaterial({ side: THREE.DoubleSide, map: new THREE.TextureLoader().load('static/mats/moss_stone.png'),transparent: true,opacity: 0.0}));
    materials.push(new THREE.MeshBasicMaterial({ side: THREE.DoubleSide, map: new THREE.TextureLoader().load('static/mats/moss_stone.png'),transparent: true,opacity: 0.0})); 
    materials.push(new THREE.MeshBasicMaterial({ side: THREE.DoubleSide, map: new THREE.TextureLoader().load('static/mats/diamond_stone.png'),transparent: false,opacity: 0.0})); 
    materials.push(new THREE.MeshBasicMaterial({ side: THREE.DoubleSide, map: new THREE.TextureLoader().load('static/mats/moss_stone.png'),transparent: true,opacity: 0.0}));  //przód
    materials.push(new THREE.MeshBasicMaterial({ side: THREE.DoubleSide, map: new THREE.TextureLoader().load('static/mats/moss_stone.png'),transparent: true,opacity: 0.0})); //tył

    var material = new THREE.MeshBasicMaterial({
        color: 0x8888ff,
        side: THREE.DoubleSide,
        map: new THREE.TextureLoader().load('static/mats/moss_stone.png'),
        transparent: true,
        opacity: 0.0
    });

    
    for(var i=0;i<8;i++){
        if(i==0|i==2|i==4|i==6){var checkpoint = new THREE.Mesh(geometry, materials);}
        else{var checkpoint = new THREE.Mesh(geometry, material);}

        if(i%2==1)checkpoint.position.y = 50;
        if(i==2|i==3|i==6|i==7)checkpoint.position.x = 50;
        if(i>3)checkpoint.position.z=50;
        container.add(checkpoint)

    }
    

    //funkcja zwracająca kontener
    this.getCheckpointCont = function () {
        return container
    }
}