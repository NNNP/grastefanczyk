`
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Container, Col, Row, Card, CardImg, CardText, CardBody, CardLink,
  CardTitle, CardSubtitle, Button } from 'reactstrap'
import maze from './maze.svg'
`

class ElementContainer extends Component
    constructor: (props) -> 
      super props
      @state = 
        gamersAmount: props.gamers
        id: props.roomId
        socket: props.socket
      @handleJoinGame = @handleJoinGame.bind this

    handleJoinGame: ->
        @state.socket.emit 'joinGame', id: @state.id

    render: ->
        <Col md={4} className="mt-3">
            <Card> 
                <CardBody> 
                    <CardTitle> Pokój nr.</CardTitle>
                    <CardSubtitle> {@state.id} </CardSubtitle>
                </CardBody>
                <div className="d-flex justify-content-center">
                    <img src={maze} width="50%" />
                </div>
                <CardBody> 
                    <Container>
                        <Row > 
                            <Col xs={8} sm={7}> Ilość Graczy </Col>
                            <Col> {@state.gamersAmount}/2 </Col>
                        </Row>
                        
                        <Row> 
                            <Col>
                                <Button className={if @state.gamersAmount >= 2 then 'disabled' else ''} onClick={@handleJoinGame}> Dołącz do gry </Button>
                            </Col>
                        </Row>
                    </Container>
                </CardBody>
            </Card>
        </Col>

export default ElementContainer