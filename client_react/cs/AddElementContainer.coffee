`
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Container, Col, Row, Card, CardImg, CardText, CardBody, CardLink,
  CardTitle, CardSubtitle, Button } from 'reactstrap'
import './AddElementContainer.css'
import plus from './positive.svg'
`

class AddElementContainer extends Component 
    constructor: (props) ->
        super props
        @state = 
            socket: props.socket
            click: props.clickAbility

    handleCardClick: (evt)->
        @state.socket.emit 'addRoom', {data: "ala"}
    
    render: ->
        <Col md={4} className="mt-3">
            <Card className="add-elem" onClick={@props.togler}>
                <CardBody>
                    <CardTitle className="text-center"> Utwórz nowy pokój </CardTitle>
                </CardBody>
                <div className="d-flex justify-content-center">
                    <img src={plus} width="50%" className="add-img"/>
                </div>
                <CardBody className="d-flex justify-content-center" >
                    <CardText className="half-size" >
                        Dołącz do gry!
                    </CardText>
                </CardBody>    
            </Card>
        </Col>

export default AddElementContainer