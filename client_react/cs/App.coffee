`
import React, { Component, Fragment } from 'react'

import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'

import { 
  Container,
  Row,
  Col,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem 
} from 'reactstrap'

import io from 'socket.io-client'

import logo from './logo.svg'
import './App.css'

import Home from './Home'
import About from './About'
import ElementContainer from './ElementContainer'
import AddElementContainer from './AddElementContainer'
import Rooms from './Constumers'
import Room from './Room'
`
class App extends Component
  constructor: (props) ->
    super props
    @state = 
      torender: "main"
      logged: false
      roomeList: []
      addOpen: false
    @socket = io()
    @socket.on 'connection', (socket) =>
      console.log socket
      undefined
    @socket.on 'rooms', (data) ->
      @setState roomeList: data
      undefined
      
    @handleFormSubmit = @handleFormSubmit.bind this
    @handleCollapseToggle = @handleCollapseToggle.bind this
    @hadnleAddModalToggle = @hadnleAddModalToggle.bind this

  handleFormSubmit: (event) ->
    console.log 'ala'
    @setState (prevState) -> 
      torender: if prevState.torender == 'main' then 'login' else 'main'
      isOpen: false
      
    event.preventDefault()
    undefined

  check: ->
    @setState torender: if @state.logged then "main" else "login" 
    
  
  hadnleAddModalToggle: ->
    console.log('test toggle')
    @setState (prevState) ->
      {addOpen: !prevState.addOpen}
  
  
  render: ->
    <Router>
      <Rooms.Provider value={@state.rooms}>
        <div className="App">
          <Switch>
            <Route exact path="/" render={ (props) => 
              <Home {...props} socket={@state.socket}/>} />
            <Route path="/room/:id/" component={ Room } />
          </Switch>
        </div>
      </Rooms.Provider>
    </Router>
    # if @state.torender == "login"
    #   <div className="full-screen">
    #     <div id="text">
    #         <Form className="container" onSubmit={@handleFormSubmit}> 
    #               <FormGroup row> 
    #                 <Label for="login" sm={2}> Login </Label>
    #                 <Col>
    #                   <Input type="login" id="login" placeholder="Podaj Login" />
    #                 </Col>
    #               </FormGroup>
    #               <Input type="submit" value="Zaloguj" />
    #         </Form>
    #     </div>
    #   </div>
    # else
    #   <Fragment>
    #   <Navbar color="dark" dark expand="md">
    #       <NavbarBrand href="/">Maze Game</NavbarBrand>
    #       <NavbarToggler onClick={@handleCollapseToggle} />
    #       <Collapse isOpen={@state.isOpen} navbar>
    #         <Nav className="ml-auto" navbar>
    #           <NavItem>
    #             <NavLink href="#">Components</NavLink>
    #           </NavItem>
    #           <NavItem>
    #             <NavLink href="#">GitHub</NavLink>
    #           </NavItem>
    #           <UncontrolledDropdown nav inNavbar>
    #             <DropdownToggle nav caret>
    #               Options
    #             </DropdownToggle>
    #             <DropdownMenu right>
    #               <DropdownItem>
    #                 Option 1
    #               </DropdownItem>
    #               <DropdownItem>
    #                 Option 2
    #               </DropdownItem>
    #               <DropdownItem divider />
    #               <DropdownItem>
    #                 Reset
    #               </DropdownItem>
    #             </DropdownMenu>
    #           </UncontrolledDropdown>
    #         </Nav>
    #       </Collapse>
    #     </Navbar>
    #     <Container> 
    #         <Row>
    #           {for value in @state.roomeList <ElementContainer gamers={value.gamers} roomId={value.id} />}
    #           <AddElementContainer socket={@state.socket} togler={@hadnleAddModalToggle}/>
    #         </Row>
    #     </Container>
    #   </Fragment>


export default App
