`
import React, { Component, Fragment } from 'react'

import { 
  Container,
  Row,
  Col,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem 
} from 'reactstrap'
import { Link } from 'react-router-dom'
import ElementContainer from './ElementContainer'
import AddElementContainer from './AddElementContainer'
`

export default class Home extends React.Component {
  constructor: (props) ->
    @state = 
      torender: "login"
      logged: false
      roomeList: []
      addOpen: false
      isOpen: false

    @handleFormSubmit = @handleFormSubmit.bind this
    @handleCollapseToggle = @handleCollapseToggle.bind this
    @hadnleAddModalToggle = @hadnleAddModalToggle.bind this

  handleFormSubmit: (event) ->
    console.log 'ala'
    @setState (prevState) -> {
      torender: if prevState.torender == 'main' then 'login' else 'main'
      isOpen: false
    }
    event.preventDefault()
    undefined
  handleCollapseToggle: ->
    @setState (prevState) ->
      isOpen: prevState.isOpen

  check: ->
    @setState {
      torender: if @state.logged then "main" else "login" 
    }

  # TODO: Context updatating 
  render: ->
    if @state.torender == "login"
        <div className="full-screen">
          <div id="text">
              <Form className="container" onSubmit={@handleFormSubmit}> 
                    <FormGroup row> 
                      <Label for="login" sm={2}> Login </Label>
                      <Col>
                        <Input type="login" id="login" placeholder="Podaj Login" />
                      </Col>
                    </FormGroup>
                    <Input type="submit" value="Zaloguj" />
              </Form>
          </div>
        </div>
      else
        <Fragment>
        <Navbar color="dark" dark expand="md">
            <NavbarBrand href="/">Maze Game</NavbarBrand>
            <NavbarToggler onClick={@handleCollapseToggle} />
            <Collapse isOpen={@state.isOpen} navbar>
              <Nav className="ml-auto" navbar>
                <NavItem>
                  <NavLink href="#">Components</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="#">GitHub</NavLink>
                </NavItem>
                <UncontrolledDropdown nav inNavbar>
                  <DropdownToggle nav caret>
                    Options
                  </DropdownToggle>
                  <DropdownMenu right>
                    <DropdownItem>
                      Option 1
                    </DropdownItem>
                    <DropdownItem>
                      Option 2
                    </DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem>
                      Reset
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </Nav>
            </Collapse>
          </Navbar>
          <Container> 
              <Row>
                {for value in @state.roomeList 
                  <ElementContainer gamers={value.gamers} roomId={value.id} />
                }
                <AddElementContainer socket={@state.socket} togler={@hadnleAddModalToggle}/>
              </Row>
          </Container>
        </Fragment>
}