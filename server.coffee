#!/usr/bin/env coffee
express = require 'express'
app = require('express')()
socket  = require 'socket.io'
http    = require('http').Server(app)
rooms   = []
mongo = require('mongodb').MongoClient
url = 'mongodb://10.8.0.1:27017/labirynt_game'

app.use('/', express.static('client_react/build'))

io = socket http

io.on 'connection', (socket) -> 
  console.log "User connect"
  socket.on "addRoom", (data) ->
    tmpTest = false
    while not tmpTest
      rand = Math.random() * 1000
      tmpTest = rooms.every (value) =>
        return !(value.id == rand)
        //#wez cos napisz XD
    tmpRoom = 
      id: rand
      user: [socket.id]
    rooms.push tmpRoom
    socket.join tmpRoom.id
    socket.emit "rooms", rooms
  socket.on 'login', (data) ->
    mongo.connect url, (err, db) ->
      if err then throw err  
      else db.createCollection data.col, (err, dat) -> 
        if err then throw err  
        else 
          dat.find {}, (err, result) ->
            test = result.every (value) =>
              return (value != data.name)
            
            if not test
              socket.emit "rooms", rooms
            db.close()
  socket.on 'rdy', (data) ->
    io.to(data.id).emit 
  socket.on 'checkpoint', (data) ->
    io.to(data.id).emit x: data.x, z: data.z
  socket.on 'joinRoom', (data) ->
    index = rooms.findIndex (value) => 
      if value.id == data.id
        return true
      else
        return false
    
    if index isnt -1
      if rooms[index].user.length < 2
        rooms[index].user.push(socket.id)
        socket.join(data.id)
        socket.emit 'connected'
  socket.on 'sign', (data) ->
    io.to(data.id).emit 'signe-send', data.sign
    

http.listen 3000,  ->
  console.log('Example app listening on port 3000!')
  undefined